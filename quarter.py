"""
Efficiently mark weak k-mers by setting a "weak bit".
Weak k-mers are k-mers that have a Hamming distance 1 neighbor in the set.
"""

from os import stat as osstat
from argparse import ArgumentParser
from datetime import datetime as dt

import numpy as np
from numba import njit, uint64, uint32, int64
from concurrent.futures import ThreadPoolExecutor, wait, FIRST_EXCEPTION, as_completed


@njit(nogil=True, locals=dict(
    elem1=uint64, elem2=uint64, h=uint64, mask=uint64, onebit=uint32))
def have_hamming_dist_one(elem1, elem2):
    """
    Return True iff DNAHammingDistance(elem1, elem2) <= 1.
    elem1, elem2 must be 2-bit encoded DNA k-mers.
    """
    mask = uint64(6148914691236517205)  # Mask 0b_01...01010101
    h = elem1 ^ elem2
    h = (h | (h >> 1)) & mask
    onebit = (h & uint64(h - 1)) == uint64(0)
    return onebit


def compile_mark_weak_kmers_in_suffix(
        block_prefix_length,
        k,):
    """
    Compile and return a function 'mark_weak_kmers_in_suffix(codes)'
    that examines a sorted array of aggregated k-mer encodings and weak bit
    and decides which k-mers are weak.
    Array 'codes' stores both kmer codes and weak bit: [kmercode|weak]

    prefix_length: Configure search for HD 1 neighbors only in blocks
        where the prefix of this length is constant
    k: the k-mer size (must be >= prefix_length)
    WEAKMASK: bit mask with a single 1-bit that marks weak k-mers
    """
    if k <= block_prefix_length:
        raise ValueError(f"k={k} <= {block_prefix_length}=block_prefix_length")
    suffix_bits = 2 * k - 2 * block_prefix_length + 1

    @njit(nogil=True, locals=dict(
        ncodes=int64, start=int64, end=int64,
        pos=int64, pos2=int64,
        prefix=uint64,
        element=uint64,
        sec_element=uint64))
    def mark_weak_kmers_in_suffix(codes):
        ncodes = codes.size
        start = end = 0
        while start < ncodes - 1:  # nothing to do if start == ncodes-1
            prefix = codes[start] >> suffix_bits
            while (end < ncodes) and (prefix == uint64(codes[end] >> suffix_bits)):
                end += 1
            for pos in range(start, end):

                element = codes[pos] >> 1
                found = False
                for pos2 in range(pos + 1, end):
                    sec_element = codes[pos2] >> 1
                    if have_hamming_dist_one(element, sec_element):
                        found = True
                        codes[pos2] |= 1
                if found:
                    codes[pos] |= 1
            start = end
    return mark_weak_kmers_in_suffix


def compile_swap_kmer_parts(partlengths, k):
    pl, il, sl = partlengths
    ibl, sbl = sl, il
    suffix_mask = (4**sl - 1)
    infix_mask = (4**il - 1) << (2 * sl)
    prefix_mask = (4**pl - 1) << (2 * (sl + il))
    SM = uint64(suffix_mask << 1)
    IM = uint64(infix_mask << 1)
    PM = uint64(prefix_mask << 1)
    suffix_back_mask = (4**il - 1)
    infix_back_mask = (4**sl - 1) << (2 * il)
    SBM = uint64(suffix_back_mask << 1)
    IBM = uint64(infix_back_mask << 1)

    @njit(nogil=True, locals=dict(
        pos=int64, code=uint64))
    def move_middle_part_right(codes):
        for pos in range(len(codes)):
            code = codes[pos]
            codes[pos] = (code & PM) | (code & 1)\
                | ((code & SM) << (2 * il))\
                | ((code & IM) >> (2 * sl))

    @njit(nogil=True, locals=dict(
        pos=int64, code=uint64))
    def build_original_kmer(codes):
        for pos in range(len(codes)):
            code = codes[pos]
            codes[pos] = (code & PM) | (code & 1)\
                | ((code & SBM) << (2 * ibl))\
                | ((code & IBM) >> (2 * sbl))

    return move_middle_part_right, build_original_kmer


# adapter function for sort
def my_sort_keys(codes, k):
    codes.sort()


@njit(nogil=True)
def get_section_borders(codes, mask, borders):
    # borders = np.empty(threads+1, dtype=np.int64)
    ncodes = codes.size
    threads = borders.size - 1
    for i in range(threads):
        borders[i] = (ncodes * i) // threads
    borders[threads] = ncodes
    for i in range(1, threads):
        while (borders[i] + 1 < ncodes) and (
                (codes[borders[i]] & mask) == (codes[borders[i] + 1] & mask)):
            borders[i] += 1
        borders[i] += 1  # first element of new block


# calculate weak k-mers ###############################
def calculate_weak_set(
        elements,
        k,
        *,
        threads=1):

    assert k < 32

    partlengths = [k // 2, k // 4, k // 4]

    if sum(partlengths) == k - 1:
        partlengths[2] += 1
    elif sum(partlengths) == k - 2:
        partlengths[1] += 1
        partlengths[2] += 1

    block_prefix_length = partlengths[0] + partlengths[1]
    m_block_prefix_length = partlengths[0] + partlengths[2]

    my_sort = my_sort_keys

    mark_weak_kmers_in_suffix = compile_mark_weak_kmers_in_suffix(block_prefix_length, k)
    mark_weak_kmers_in_middle = compile_mark_weak_kmers_in_suffix(m_block_prefix_length, k)
    swap_right, swap_back = compile_swap_kmer_parts(partlengths, k)

    with ThreadPoolExecutor(max_workers=1 + threads) as executor:
        # Count k-mers for each combination of (subtable, prefix, nextchars)

        section_borders = np.empty(threads + 1, dtype=np.int64)

        block_prefix_bits = partlengths[0] + partlengths[1]
        block_prefix_shift = 2 * partlengths[2] + 1
        block_prefix_mask = uint64((4**block_prefix_bits - 1) << block_prefix_shift)
        get_section_borders(elements, block_prefix_mask, section_borders)

        # first weak marking
        futures = [
            executor.submit(
                mark_weak_kmers_in_suffix,
                elements[section_borders[i]:section_borders[i + 1]])
            for i in range(threads)]
        for f in as_completed(futures):
            if f.exception() is not None:
                raise Exception(f"In future {f}: {f.exception()}")

        # Swap the middle and the right part
        futures = [
            executor.submit(swap_right, elements[section_borders[i]:section_borders[i + 1]])
            for i in range(threads)]
        done, not_done = wait(futures, return_when=FIRST_EXCEPTION)
        assert len(not_done) == 0

        # Re-sort after bit-swap
        block_prefix_bits = partlengths[0]
        block_prefix_shift = 2 * (partlengths[1] + partlengths[2]) + 1
        block_prefix_mask = uint64((4**block_prefix_bits - 1) << block_prefix_shift)
        get_section_borders(elements, block_prefix_mask, section_borders)

        futures = []
        for i in range(threads):
            sort_values = elements[section_borders[i]:section_borders[i + 1]]
            futures.append(executor.submit(my_sort, sort_values, k))

        for f in as_completed(futures):
            if f.exception() is not None:
                raise Exception(f"In future {f}: {f.exception()}")

        # Define sections, mark k-mers (from middle part)
        block_prefix_bits = partlengths[0] + partlengths[2]
        block_prefix_shift = 2 * partlengths[1] + 1
        block_prefix_mask = uint64((4**block_prefix_bits - 1) << block_prefix_shift)
        get_section_borders(elements, block_prefix_mask, section_borders)

        # # Second weak marking
        futures = [
            executor.submit(
                mark_weak_kmers_in_middle,
                elements[section_borders[i]:section_borders[i + 1]])
            for i in range(threads)]
        for f in as_completed(futures):
            if f.exception() is not None:
                raise Exception(f"In future {f}: {f.exception()}")

        # Bit-swap k-mers back
        futures = [
            executor.submit(swap_back, elements[section_borders[i]:section_borders[i + 1]])
            for i in range(threads)]
        for f in as_completed(futures):
            if f.exception() is not None:
                raise Exception(f"In future {f}: {f.exception()}")

    # That's all, folks. It ends here.


def save_marked(codes, output, k, threads):
    """
        The weak mark is set in the least significant bit.
        For the check we move it the the k * 2 + 1 bit.
    """
    weak_bit = uint64(4 ** k)

    @njit(nogil=True)
    def move_marking(codes, start, end):
        for i in range(start, end):
            codes[i] = (codes[i] >> 1) | weak_bit if codes[i] & 1 else codes[i]

    with ThreadPoolExecutor(max_workers=threads) as executor:
        starts = np.array([len(codes) // threads * i for i in range(threads)] + [len(codes)], dtype=np.int64)

        futures = [executor.submit(
            move_marking, codes, starts[i], starts[i + 1]
        ) for i in range(len(starts) - 1)]

        wait(futures)

    np.save(output, codes)


def shift_codes(codes, threads):
    """
        The weak is marked at the least significant bit.
        We shift all codes by 1 to the left
    """

    @njit(nogil=True)
    def move_marking(codes, start, end):
        for i in range(start, end):
            codes[i] = codes[i] << 1

    with ThreadPoolExecutor(max_workers=threads) as executor:
        starts = np.array([len(codes) // threads * i for i in range(threads)] + [len(codes)], dtype=np.int64)

        futures = [executor.submit(
            move_marking, codes, starts[i], starts[i + 1]
        ) for i in range(len(starts) - 1)]

        wait(futures)
        for future in futures:
            try:
                _ = future.result()
            except Exception:
                raise future.exception()


def load_array(fname):
    fsize = osstat(fname).st_size  # file size in bytes
    assert fsize % 8 == 0
    n_uint64 = fsize // 8
    arr = np.empty(n_uint64, dtype=np.uint64)
    b = arr.view(np.uint8)
    assert b.size == fsize
    with open(fname, "rb") as fin:
        fin.readinto(b)
    return arr


def main(args):
    kmer_codes = load_array(args.numpy)
    shift_codes(kmer_codes, args.threads)
    k = args.k

    start_weak = dt.now()
    calculate_weak_set(kmer_codes, k, threads=args.threads)
    end_weak = dt.now()
    elapsed_sec = (end_weak - start_weak).total_seconds()

    if args.log is not None:
        with open(args.log, "wt") as lfile:
            print(f"quarter,{k},{args.threads},{elapsed_sec}", file=lfile)
    else:
        print("\n## Result")
        print(f"quarter,{k},{args.threads},{elapsed_sec}\n")

    if args.output:
        save_marked(kmer_codes, args.output, k, args.threads)


def get_argument_parser():
    p = ArgumentParser("Load an index and calculate weak k-mers using an adapted quicksort.")
    p.add_argument("--numpy", help="Numpy array of integer encoded k-mers")
    p.add_argument("-k", required=True, type=int,
                   help="Size of the k-mers in")
    p.add_argument("--threads", "-T", default=1, type=int,
        help="Number of threads")
    p.add_argument("--output", help="output all k-mers and weak markings")
    p.add_argument("--log", help="file to print runtimes")
    return p


if __name__ == '__main__':
    p = get_argument_parser()
    args = p.parse_args()
    main(args)
