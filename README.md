# Swiftly identifying strongly unique $k$-mers

## Introduction
This repository contains the algorithms presented in the paper "Swiftly identifying strongly unique $k$-mers".
The implementation uses just-in-time compiled Python using the numba library.

## Setup
We provide an environment for conda/mamba to enable easy execution.
We recommend to install conda, mamba and Python from [miniforge](https://github.com/conda-forge/miniforge).
Once installed, running a short test is quick and easy with the followin instructions.

To create the environment, run:
```
mamba env create
conda activate strong
```

## Data

### Small *E. coli* data set
As an example, we provide a small numpy array of all 25-mers of the *E. coli* reference genome (`ecoli_k25.npy`).

### Human reference genome (t2t) 25-mers
The dataset used for evaluation in the paper is the human t2t reference genome.
The sorted numpy array of the 25-mers and their reverse complements of the t2t genome can be downloaded here (**CAUTION! 36 GB**):
https://kingsx.cs.uni-saarland.de/index.php/s/CfFt7FkZcox6CJ6


## Running the algorithms
We provide a brief explanation and examples of how the different algorithms can be executed.

### QUARTER
To run the QUARTER algorithm, you need to specify the input (`--numpy FILE.npy`), the $k$-mer size (`-k INTEGER`) and the number of threads (`-T INTEGER`).
For the *E. coli* dataset, using 8 threads, run:
```
python quarter.py --numpy ecoli_k25.npy -k 25 -T 8
```

### FOURWAY (+ PAIRWISE)
To run the FOURWAY algorithm, you need to specify the input (`--numpy FILE.npy`), the $k$-mer size (`-k INTEGER`) and the number of threads (`-T INTEGER`).
In addition, you can specify the interval size for which the pairwise comparison is computed using `--pairwise-limit LIMIT`. 
If this is set to 0, the FOURWAY algorithm without pairwise comparison will be executed.

For the *E. coli* dataset, using 8 threads, run:
```
python fourway.py --numpy ecoli_k25.npy -k 25 -T 8 --pairwise-limit 24
```
