"""
weak_fourway.py:
Weak k-mer identification (from a sorted array with k-mers and reverse complements)
by using a 4-way comparison strategy analogous to mergesort.
"""

from os import stat as osstat
from argparse import ArgumentParser
from datetime import datetime as dt
from concurrent.futures import ThreadPoolExecutor, wait

import numpy as np
from numba import njit, uint8, uint32, int32, int64, uint64


# OLD CODEBASE from Jens' implementation
# but enhanced for variable block_size and Tpairwise

def calc_weak_end_old(elements, k, executor, block_size=2, Tpairwise=8):
    weak_mask = uint64(4**k)
    kmer_mask = uint64(4**k - 1)  # mask for the entire kmer
    Hmask = uint64(6148914691236517205)  # Mask 0b_01...01010101
    if block_size >= k // 2:
        raise ValueError(f"ERROR: must have {block_size=} < {k//2=}")

    @njit(nogil=True, locals=dict(
        elem1=uint64, elem2=uint64, h=uint64, onebit=int32))
    def have_hamming_dist_one(elem1, elem2):
        """
        Return True iff DNAHammingDistance(elem1, elem2) <= 1.
        elem1, elem2 must be 2-bit encoded DNA k-mers.
        """
        h = elem1 ^ elem2
        h = (h | (h >> 1)) & Hmask
        onebit = (h & uint64(h - 1)) == uint64(0)
        return onebit

    @njit(nogil=True, locals=dict())
    def build_blocks(elements, starts):
        """
        Find the starting indices for each bucket in the weak quicksort algorithm.
        -  elements (array): The array of elements to be sorted.
        -  starts (array): (length 5) start indices of sub-buckets
        - block_size (int): number of equal base pairs at the start -> 4^block_size different blocks
        """
        mask = uint64(4 ** block_size - 1) << 2 * (k - block_size)
        starts[0] = 0
        writer = 1
        for i in range(0, len(elements) - 1):
            base1 = elements[i] & mask
            base2 = elements[i + 1] & mask

            if base1 != base2:
                starts[writer] = i + 1
                writer += 1
        starts[writer] = len(elements)
        writer += 1
        return writer

    @njit(nogil=True, locals=dict())
    def _find_starts(elements, start, end, pos, starts):
        """
        Find the starting indices for each bucket in the weak quicksort algorithm.
        -  elements (array): The array of elements to be sorted.
        -  starts (array): (length 5) start indices of sub-buckets
        """
        starts[:] = start
        for i in range(start, end - 1):
            base1 = (elements[i] >> (2 * pos)) & 3
            base2 = (elements[i + 1] >> (2 * pos)) & 3
            if base1 == 3:
                starts[base1] = i
                break
            elif base1 != base2:
                starts[base1 + 1:] = i + 1
        starts[base2 + 1:] = end
        # Sven: TODO?

    @njit(nogil=True, locals=dict(smallest_index=int64, mask=uint64,
                                  weak_mask=uint64, moveable_pointer_mask=uint8, min_indices=uint8,
                                  min_value=uint64, current_bit=uint8))
    def compare(elements, pos, starts, pointers):
        mask = uint64(uint64(4 ** k - 1) ^ uint64(3 << (2 * pos)))
        pointers[:] = starts[:4]

        while True:
            min_indices = 0
            min_value = 4**k
            for p in range(4):
                if elements[pointers[p]] & mask < min_value and pointers[p] != starts[p + 1]:
                    min_value = elements[pointers[p]] & mask
                    min_indices = 1 << p
                elif elements[pointers[p]] & mask == min_value:
                    min_value = min_value
                    min_indices |= (1 << p)

            if min_indices == 0:
                break

            n_is_0_or_1 = (min_indices & (min_indices - 1)) != 0

            for p in range(4):
                current_bit = 1 << p
                do_stuff = (min_indices & current_bit) != 0
                elements[pointers[p]] |= weak_mask * n_is_0_or_1 * do_stuff
                pointers[p] += (pointers[p] < starts[p + 1]) * do_stuff

    @njit(nogil=True, locals=dict(
        kmeri=uint64, kmerj=uint64,
        foundi=uint32, foundj=uint32))
    def _run_pairwise(elements, start, end):
        for i in range(start, end - 1):
            kmeri = elements[i] & kmer_mask
            foundi = 0
            for j in range(i + 1, end):
                kmerj = elements[j] & kmer_mask
                found = have_hamming_dist_one(kmeri, kmerj)
                foundi |= found
                elements[j] |= uint64(weak_mask * found)
            elements[i] |= uint64(weak_mask * foundi)

    @njit(nogil=True, locals=dict(start=int64, end=int64))
    def _run_recursive(elements, start, end, pos):
        if end - start <= Tpairwise:
            _run_pairwise(elements, start, end)
            return
        starts = np.zeros(5, dtype=np.uint64)
        pointers = np.zeros(4, dtype=np.uint64)
        _find_starts(elements, start, end, pos, starts)
        compare(elements, pos, starts, pointers)
        if pos > 0:
            for i in range(4):
                if starts[i + 1] - starts[i] >= 2:
                    _run_recursive(elements, starts[i], starts[i + 1], pos - 1)

    @njit(nogil=True, locals=dict(block_start=int64, block_end=int64,
                                  block_mask=uint64))
    def _run(elements, start, end, pos):
        # iterate through the block start -> end and always do a merge for a block with
        # k - pos equal bases at the start
        block_start = start
        block_mask = uint64(4 ** (k - pos) - 1) << uint64(2 * pos)
        while block_start < end:
            block = elements[block_start] & block_mask
            for i in range(block_start, end):
                if elements[i] & block_mask != block:
                    block_end = i
                    break
            else:
                block_end = end
            _run_recursive(elements, block_start, block_end, pos - 1)
            block_start = block_end

    starts = np.zeros(4 ** block_size + 1, dtype=np.int64)
    end = build_blocks(elements, starts)

    futures = [executor.submit(_run, elements, starts[s], starts[s + 1], k // 2 + 1) for s in range(end - 1)]
    wait(futures, return_when="ALL_COMPLETED")
    for future in futures:
        try:
            _ = future.result()
        except Exception:
            raise future.exception()


# NEW codebase implemented by Sven with branchless code
# It seems to perform worse in practice.
def calc_weak_end(elements, k, executor, block_size=2, Tpairwise=8):
    """
    Parameters:
    - elements: ndarray (integer-encoded k-mers)
    - k: int (k-mer size)
    - executor: thread pool executor
    - block_size: int (number of initial nucleotides to create blocks for threads)
    """
    weak_mask = uint64(4**k)  # mask for the weak bit (higher than k-mer bits)
    kmer_mask = uint64(4**k - 1)  # mask for the entire kmer
    Hmask = uint64(6148914691236517205)  # Mask 0b_01...01010101
    if block_size >= k // 2:
        raise ValueError(f"ERROR: must have {block_size=} < {k//2=}")

    @njit(nogil=True, inline='always', locals=dict(
        elem1=uint64, elem2=uint64, h=uint64, onebit=uint32))
    def have_hamming_dist_one(elem1, elem2):
        """
        Return True iff DNAHammingDistance(elem1, elem2) <= 1.
        elem1, elem2 must be 2-bit encoded DNA k-mers.
        """
        h = (elem1 ^ elem2) & kmer_mask
        h = (h | (h >> 1)) & Hmask
        onebit = (h & uint64(h - 1)) == uint64(0)
        return onebit

    @njit(nogil=True, locals=dict(
        mer1=uint64, mer2=uint64))
    def build_blocks(elements, starts):
        """
        Find the starting indices of each block in the FourWay algorithm.
        A block is an interval of `elements` where all k-mers start with the same b-mer,
        where b is block_size (typically 2).
        - elements (array): The array of elements (k-mers) to be partiioned into blocks
        - starts (array, output): start indices of blocks;
          must be big enough to take up to 4**b + 1 block starts.
          We may not use all of them (some blocks may be empty); this is ok.
        The last block start is n, wehere n is the number of elements
        Return the number of used block starts.
        """
        n = elements.size
        mask = uint64(4**block_size - 1) << (2 * (k - block_size))
        starts[0] = 0
        nblocks = 1
        for i in range(0, n - 1):
            mer1 = elements[i] & mask
            mer2 = elements[i + 1] & mask
            if mer1 != mer2:
                starts[nblocks] = i + 1
                nblocks += 1
        starts[nblocks] = n
        nblocks += 1
        # here: starts[0] == 0 and starts[nblocks-1] == n
        return nblocks

    @njit(nogil=True, locals=dict(
        nucx=int32, nuci=int32))
    def _find_starts(elements, start, end, pos, starts):
        """
        Find the starting indices for each bucket
        within a block (start:end) in the FourWay algorithm,
        based on the nucleotide at position pos.
        -  elements (array): The array of elements to be sorted.
        -  starts (array): (length 5) start indices of sub-buckets
        """
        zpos = 2 * pos
        nucx = -1  # initial illegal nucleotide value
        for i in range(start, end):
            nuci = int32(elements[i] >> zpos) & int32(3)
            while nucx < nuci:
                nucx += 1
                starts[nucx] = i
        while nucx < 4:
            nucx += 1
            starts[nucx] = end

    @njit(nogil=True, locals=dict(
        mask=uint64, min_value=uint64,
        active=uint32, min_indices=uint32,
        val=uint64, a=uint32,
        found=uint32, do_stuff=uint32))
    def compare(elements, pos, starts, pointers):
        # Define a mask that will set the 2 bits for position pos to 00,
        # but otherwise has 2k bits set to 1
        # mask = uint64(kmer_mask ^ uint64(3 << (2 * pos)))
        # Define a mask that will only look at positions (pos-1) ... 1 0,
        # and ignore position pos and everything beyone.
        mask = uint64(4**pos - 1)
        # Copy the buckets starts into the pointers
        pointers[:4] = starts[:4]
        while True:
            active = 0
            for p in range(4):
                active |= uint32(1 << p) * uint32(pointers[p] < starts[p+1])
            if active & uint32(active - 1) == uint32(0):
                break  # done if there is a single active bucket left
            min_indices = 0  # indices of min values
            min_value = uint64(4**k)
            for p in range(4):
                val = elements[pointers[p]] & mask
                a = ((active & (1<<p)) != 0)
                newmin =  a and (val < min_value)
                nonewmin = not newmin
                samemin = a and (val == min_value)
                min_value = uint64(val * newmin) | uint64(min_value * nonewmin)
                min_indices ^= min_indices * uint32(newmin)
                min_indices |= (1 << p) * (newmin or samemin)
            # check whether any weak were found (popcount(min_indices) >= 2)
            found = (uint32(0) != (min_indices & uint32(min_indices - 1)))
            # set weak bits (if necessary), and
            # increment pointers of min_indices
            for p in range(4):
                do_stuff = ((min_indices & (1 << p)) != 0)
                elements[pointers[p]] |= uint64(weak_mask * uint64(found * do_stuff))
                pointers[p] += do_stuff

    @njit(nogil=True, locals=dict(
        kmeri=uint64, kmerj=uint64,
        foundi=uint32, foundj=uint32))
    def _run_pairwise(elements, start, end):
        for i in range(start, end - 1):
            kmeri = elements[i]
            foundi = 0
            for j in range(i + 1, end):
                found = have_hamming_dist_one(kmeri, elements[j])
                elements[j] |= uint64(weak_mask * found)
                foundi |= found
            elements[i] |= uint64(weak_mask * foundi)

    @njit(nogil=True, locals=dict(start=int64, end=int64))
    def _run_recursive(elements, start, end, pos):
        if end - start <= Tpairwise:
            _run_pairwise(elements, start, end)
            return
        starts = np.zeros(5, dtype=np.int64)
        pointers = np.zeros(4, dtype=np.int64)
        _find_starts(elements, start, end, pos, starts)
        compare(elements, pos, starts, pointers)
        if pos == 0:
            return
        for i in range(4):
            if starts[i + 1] - starts[i] >= 2:
                _run_recursive(elements, starts[i], starts[i + 1], pos - 1)

    @njit(nogil=True, locals=dict(
        block_start=int64, block_end=int64,
        agreemask=uint64, blockmer=uint64))
    def _run(elements, start, end, firstpos):
        # the work of one job (done by one thread):
        # Iterate through the big block from `start` to `end`,
        # to locate all sub-blocks [block_start:block_end],
        # where all initial nucleotides at positions
        # (k-1) ... (firstpos+1) agree (k-1-(firstpos+1)+1 positions);
        # we recursively compare for a difference at positions
        # (firstpos) ... (1) (0)
        # k = 25: (12 + 13) positions (24..13 | 12..0)
        # agreepos = 12 = 25 - 12 - 1; firstpos = 12 = 25 // 2
        # agreemask = 11..11|00..00 (firstpos+1 00 pairs)
        block_start = start
        agreepos = k - firstpos - 1
        agreemask = uint64(4**agreepos - 1) << (2 * (firstpos + 1))
        while block_start < end:
            blockmer = uint64(elements[block_start] & agreemask)
            for i in range(block_start + 1, end):
                if uint64(elements[i] & agreemask) != blockmer:
                    block_end = i
                    break
            else:
                block_end = end
            _run_recursive(elements, block_start, block_end, firstpos)
            block_start = block_end

    # main code of calc_weak_end():
    # compute start positions of all blocks
    starts = np.zeros(4**block_size + 1, dtype=np.int64)  # start positions
    nblocks = build_blocks(elements, starts)
    # We consider only 3rd and 4th quarter; positions in the k-mer are indexed like
    # (k-1) (k-2) ... 1 0;
    # to make sure the "right half" contains k//2 (k even) or k//2 + 1 (k odd) positions,
    # we must begin the 4-way comparison at the variable position
    # (k//2 - 1) for even k, or at k//2 for odd k.
    firstpos = k // 2 - (k % 2 == 0)
    futures = [executor.submit(_run,
                   elements, starts[s], starts[s + 1], firstpos)  # elements, start, end, pos
               for s in range(nblocks - 1)]
    wait(futures, return_when="ALL_COMPLETED")
    for future in futures:
        try:
            _ = future.result()
        except Exception:
            raise future.exception()


def load_array(fname):
    """load numpy array from file `fname`"""
    fsize = osstat(fname).st_size  # file size in bytes
    if fsize % 8 != 0:
        raise RuntimeError(f"ERROR: file size in bytes of '{fname}' is not divisible by 8 bytes!")
    n_uint64 = fsize // 8
    arr = np.empty(n_uint64, dtype=np.uint64)
    b = arr.view(np.uint8)
    assert b.size == fsize, "INTERNAL ERROR"
    with open(fname, "rb") as fin:
        fin.readinto(b)
    return arr


def oldmain(args):
    k = args.k
    Tpairwise = args.pairwise_limit
    bs = args.block_nucleotides
    kmer_codes = load_array(args.numpy)
    with ThreadPoolExecutor(max_workers=args.threads) as executor:
        start_weak = dt.now()
        calc_weak_end_old(kmer_codes, k, executor, block_size=bs, Tpairwise=Tpairwise)
        end_weak = dt.now()
        elapsed_sec = (end_weak - start_weak).total_seconds()
        if args.log is not None:
            with open(args.log, "wt") as lfile:
                print(f"merge,{k},{args.threads},{elapsed_sec}", file=lfile)
        else:
                print(f"merge,{k},{args.threads},{elapsed_sec}")
        if args.output:
            np.save(args.output, kmer_codes)


def main(args):
    k = args.k
    Tpairwise = args.pairwise_limit
    bs = args.block_nucleotides
    print("# Weak FourWay\n")
    print(f"- k={k}; numpy array: {args.numpy}")
    print(f"- using {args.threads} threads")
    print(f"- block nucleotides: {bs}, i.e. {4**bs} jobs for threads")
    print(f"- pairwise comparison for intervals of length <= {Tpairwise}")
    print(f"- log file: {args.log}")
    print(f"- output array file: {args.output}")
    # Read the numpy array from the --numpy argument
    print(f"- reading array file...")
    kmer_codes = load_array(args.numpy)
    # Identify the weak k-mers using FourWay on the 3rd and 4th quarters
    with ThreadPoolExecutor(max_workers=args.threads) as executor:
        start_weak = dt.now()
        print(f"- starting weak {k}-mer identification")
        calc_weak_end(kmer_codes, k, executor, block_size=bs, Tpairwise=Tpairwise)
        end_weak = dt.now()
    elapsed_sec = (end_weak - start_weak).total_seconds()
    print(f"- done after {elapsed_sec:.2f} seconds")
    # Write log file with running time information
    if args.log is not None:
        with open(args.log, "wt") as lfile:
            print(f"fourway,{k},{args.threads},{elapsed_sec}", file=lfile)
    else:
            print("\n## Result")
            print(f"fourway,{k},{args.threads},{elapsed_sec}\n")
    # Write output file if desired
    if args.output:
        print(f"- saving resulting array...")
        np.save(args.output, kmer_codes)
    print(f"- done.")


def get_argument_parser():
    p = ArgumentParser("Identify weak DNA k-mers using a 4-way comparison")
    p.add_argument("--numpy", required=True,
        help="sorted numpy array of integer encoded k-mers and their reverse complements")
    p.add_argument("-k", required=True, type=int,
        help="k-mer size")
    p.add_argument("--threads", "-T", type=int, default=1,
        help="Number of threads")
    p.add_argument("--log",
        help="name of log file to collect running times")
    p.add_argument("--pairwise-limit", "-P", type=int, default=8,
        help="Interval length below which to use the pairwise comparison [8]")
    p.add_argument("--block-nucleotides", "-B", type=int, default=5,
        help="Number of nucleotides for thread block definition [5]")
    p.add_argument("--output", "-o",
        help="name of file to output integer-encoded k-mers with their weak marks")
    p.add_argument("--oldcode", action="store_true",
        help="run old branching code (for backward reproducibility)")
    return p


if __name__ == '__main__':
    p = get_argument_parser()
    args = p.parse_args()
    if args.oldcode:
        oldmain(args)
    else:
        main(args)
